# How to setup Selenium-Grid in EC2 instance using Terraform and run cucumber test in it
![small](/project_diagram.png)
### Used tools: 
- IaC - Terraform
- Cloud Provider: AWS
- Language,Test tool: Java, Maven, Selenium and Cucumber 
- CI/CD : Jenkins