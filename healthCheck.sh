#!/bin/bash

#usage method
display_usage() { 
	echo -e "\nUsage: $0 -u [url] -i [iteration] \n" 
} 

#chek status
checkStatus(){
    echo "Givel url is: $url"
    echo "Iteration: $iteration"
    START=1
    END=${iteration}
    for ((c=$START; c<=$END; c++))
    do
    status=$(curl -s -o /dev/null ${url} -w '%{http_code}')
    if [ ${status} == 200 ]
    then
        break
    else
        echo "TRY:${c} WAITING for 5 seconds........"
        sleep 5s
    fi
    done

    if [ ${status} == 200 ]
    then
        echo "Selenium grid: OK!"
    else
        echo "Selenium grid: FAILED!"
    fi
}


#----------------------------------------------------------
# if less than two arguments supplied, display usage 
	if [[  $# -le 3 || $# -ge 5 ]]
	then 
		display_usage
		exit 1
	fi 
 
# check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 


while getopts u:i: flag
do
    case "${flag}" in
        u) url=${OPTARG};;
        i) iteration=${OPTARG};;
    esac
done

if [[ ( ${1} == "-u") &&  ${3} == "-i"  &&  ${4} -ge 1 ]] 
then 
	checkStatus
else
    display_usage
    exit 0
fi 


