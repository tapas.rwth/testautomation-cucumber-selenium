#terraform config
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.10.0"
    }
  }
}

#provider
provider "aws" {
  //profile = "default"
  region  = "eu-central-1"
}

#fetch the ami id in the desired region
data "aws_ami" "getami" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.20210219.0-x86_64-gp2"]
  }

}


#create aws ec2 instance
resource "aws_instance" "my-ec2" {
  ami           = data.aws_ami.getami.id
  instance_type = var.instancetype
  key_name               = aws_key_pair.loginkey.key_name
  vpc_security_group_ids = [aws_security_group.sg_selenium.id]
  tags = {
    Name = "Selenium-grid"
  }
  availability_zone = var.az
  user_data         = file("${path.module}/bootstrapscript.sh")

  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = file("${path.module}/${var.ami-key-pair-name}.pem")
    host = self.public_ip
    timeout = "5m"
    agent = false
  }
  provisioner "file" {
    source      = "../docker-compose.yml"
    destination = "/home/ec2-user/docker-compose.yml"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 60"
    ]
  }


}

#produce ec2 instance public ip address in the output
output "instanceip" {
  value = aws_instance.my-ec2.public_ip
}
resource "null_resource" "write_properties" {
  provisioner "local-exec" {
    command = "echo public_ip=${aws_instance.my-ec2.public_ip} > ./ec2.properties"
  }
}

resource "null_resource" "dockercompose_version" {
  provisioner "local-exec" {
    command = "ssh -i ${path.module}/${var.ami-key-pair-name}.pem -o StrictHostKeyChecking=no ec2-user@${aws_instance.my-ec2.public_ip} 'while ! docker-compose -version; do sleep 5; done'"
  }
  }
/*
resource "null_resource" "docker_command" {
  provisioner "local-exec" {
    command = "ssh -i ${path.module}/${var.ami-key-pair-name}.pem -o StrictHostKeyChecking=no ec2-user@${aws_instance.my-ec2.public_ip} 'docker-compose up -d'"
  }
}*/