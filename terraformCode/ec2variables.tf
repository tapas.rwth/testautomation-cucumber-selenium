#variable for instance type
variable "instancetype" {
  type    = string
  default = "t2.micro"
}

#variable to hold az
variable "az" {
  type    = string
  default = "eu-central-1a"
}

#variable for login key-pair name
variable "ami-key-pair-name" {
  type    = string
  default = "login-keypair-seleniumgrid"
}

#allowed port list in ingress rules for security group
variable "sg_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = [22, 4444]
}

