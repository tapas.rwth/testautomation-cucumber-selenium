package StepDefinition;

import Common.Base;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import Pages.*;

import java.net.MalformedURLException;

public class Openmainpage extends Base {


    @Given("User goes to cucumber mainpage")
    public void openMainPage() throws InterruptedException, MalformedURLException {
        setUp();
        Thread.sleep(3000);
        System.out.println("Visited Page Title: "+driver.getTitle());

    }
    @When("User clicks on view Docs")
    public void clickViewDocs() throws InterruptedException{
        MainPage mainPage=new MainPage();
        mainPage.OpenDocs(driver);
        Thread.sleep(3000);
        System.out.println("Visited Page Title: "+driver.getTitle());

    }
    @Then("User closes the browser")
    public void userLogsOut() {
        driver.quit();

    }
}
