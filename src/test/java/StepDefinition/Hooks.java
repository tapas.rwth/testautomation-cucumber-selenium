package StepDefinition;
import Common.Base;
import io.cucumber.java.After;

public class Hooks extends Base{

    @After
    public void closeBrowser(){
        tearDown();
    }
}
