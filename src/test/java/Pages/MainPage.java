package Pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class MainPage{
    private By viewDocs= By.cssSelector("a[title*='View Docs']");

    public void OpenDocs(WebDriver driver) {
        driver.findElement(viewDocs).click();
    }

}
