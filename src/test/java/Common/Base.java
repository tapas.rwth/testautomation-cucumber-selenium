package Common;
import org.openqa.selenium.WebDriver;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class Base {
    public String baseURL = "https://cucumber.io/";
    public static WebDriver driver;
    Browserconfig browserconfig = new Browserconfig();

    public void setUp() throws MalformedURLException {
        driver = browserconfig.SelectBrowser();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseURL );
    }

    public void tearDown() {
        if (driver != null){
            driver.quit();
    }
    }
}
