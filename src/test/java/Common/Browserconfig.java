package Common;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;


public class Browserconfig {
    public static WebDriver driver;
    //private static String hubUrl = "http://localhost:4444/wd/hub";
    private static final String hubUrl = System.getProperty("hubUrl");
    private static Boolean set_headless = false;
    public WebDriver SelectBrowser() throws MalformedURLException {
        String selectBrowser = System.getProperty("browser","local-chrome");
        System.out.println("Selected Browser: "+ selectBrowser );
        switch (selectBrowser) {
            case "local-firefox":
                return LocalFirefox(set_headless);
            case "local-chrome":
                return LocalChrome(set_headless);
            case "grid-chrome":
                return GridChrome();
            case "grid-firefox":
                return GridFirefox();

        }
        return null;
    }

    private WebDriver LocalFirefox(Boolean set_headless){
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = FirefoxOptionsSettings(set_headless);
        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    private WebDriver LocalChrome(Boolean set_headless){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = ChromeOptionSettings(set_headless);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    public WebDriver GridChrome() throws MalformedURLException {
        System.out.println("Selenium Hub url: "+ hubUrl );
        ChromeOptions options = ChromeOptionSettings(true);
        driver = new RemoteWebDriver(new URL(hubUrl), options);
        driver.manage().window().setSize(new Dimension(1920,1080));
        return driver;
    }
    public WebDriver GridFirefox() throws MalformedURLException {
        System.out.println("Selenium Hub url: "+ hubUrl );
        FirefoxOptions options = FirefoxOptionsSettings(true);
        driver = new RemoteWebDriver(new URL(hubUrl), options);
        driver.manage().window().setSize(new Dimension(1920,1080));
        return driver;
    }

    //**********firefox options***********
    private static FirefoxOptions FirefoxOptionsSettings(boolean headless){
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("javascript.enabled", true);
        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);
        options.addArguments("--private");
        options.setHeadless(headless);
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
        return options;

    }

    //**********chrome options***********
    private static ChromeOptions ChromeOptionSettings(boolean headless){
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension",false);
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        options.addArguments("incognito");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        options.setHeadless(headless);
        return options;

    }




}
