package TestRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/FeatureFile",
        plugin = { "json:target/cucumber-reports/cucumber.json",
                "pretty", "html:target/cucumber-reports/cucumber-html-reports.html",
                "junit:target/cucumber-reports/cucumber.xml" },
                    glue = "StepDefinition",
                    tags="@Test")


public class TestRunner {

}
